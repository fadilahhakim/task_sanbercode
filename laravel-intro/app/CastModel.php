<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CastModel extends Model
{
   protected $casts = ['name','umur','bio'];
}
