<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup(){
        return view('register');
    }

    public function welcome(Request $request){
        $first = $request['firstname'];
        $last =$request['lastname'];

        return view('welcome', ['first'=>$first,'last'=>$last]);
    }
}
