<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CastController extends Controller
{
   public function create(){
    
        return view('casts.create');
   }


    public function store(Request $request)
    {
    //    dd($request->all());

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required|min:6',
        ],
        [
            'nama.required'=>'nama tidak boleh kosong harus di isi',
            'umur.required'=>'umur tidak boleh kosong harus di isi',
            'bio.required'=>'bio harus di isi',
            'bio.min'=>'bio minimal 6 karakter'
        ]);

        DB::table('casts')->create(
            [
                'name' => $request['nama'], 
                'umur' => $request['umur'],
                'bio' =>  $request['bio']
            
            ]
        );

        return redirect ('/cast');

    }

    public function index()
    {
        $casts = DB::table('casts')->latest()->get();
        return view('casts.index', ['casts'=>$casts]);
    }

    public function show()
    {
        $cast = DB::table('casts')->where('id', $id)->first();

    }

}
