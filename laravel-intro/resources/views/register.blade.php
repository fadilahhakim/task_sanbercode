<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>register page</title>
    </head>
    <body>
        <div class="wraper">
            <h1>Buat Acount Baru</h1>
            <h3>Sign Up Form</h3>

            <form action="/welcome" method="post">
                @csrf
                <label>Fisrt Name&ensp;:</label>
                <input type="text" name="firstname"><br><br>
                <label>Last Name&ensp;:</label>
                <input type="text" name="lastname"><br><br>
                <label>Password &ensp;:&nbsp;  </label>
                <input type="password" name="pass"><br><br>
                <label>Gender:</label><br><br>
                <select name="national">
                    <option value="Male">Laki-laki</option>
                    <option value="Female">Perempuan</option>
                </select><br><br>
                <label>Laguage Spoken</label><br>
                <input type="checkbox" name="skill">Bahasa Indonesia <br>
                <input type="checkbox" name="skill">English <br><br>     
                <input type="submit" value="submit">
            </form>

        </div>
    </body>
</html>
