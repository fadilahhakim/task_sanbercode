@extends('layouts.master')
@section('title')
Cast
@endsection

@section('card-title')
  List Cast
@endsection



@section('content')
<a href="cast/create">
  <button type="button" class="btn btn-info">Create new cast</button>
</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($casts as $cast => $key)
      <tr>
       <td>{{$key->name}}</td>
       <td>{{$key->umur}}</td>
       <td>{{$key->bio}}</td>
       <td>
        <a href="cast/{{$key->id}}">
          <button type="button" class="btn btn-info">Update</button>
        </a>
        <a href="">
          <button type="button" class="btn btn-info">Delete</button>
        </a>
       </td>
      </tr>
      @endforeach
      @if (count($casts)<=0)
        <tr>
        <td>tidak ada data</td>
       </tr>
      @endif
        
  
       
   </tbody>
  
  </table>

@endsection