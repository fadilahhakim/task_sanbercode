@extends('layouts.master')
@section('title')
Cast
@endsection

@section('card-title')
   Cast Form
@endsection

@section('content')
<form  method="post" action="/cast">
  @csrf
   <div class="form-group">
     <label>Name :</label>
     <input class="form-control" type="text" name="nama" value="{{old('nama')}}">
   </div>
   @error('nama')
       <div class="alert alert-danger">{{$message}}</div>
   @enderror  
   <div class="form-group">
     <label>Umur :</label>
     <input class="form-control" type="text" name="umur" value="{{old('umur')}}">
   </div>
   @error('umur')
       <div class="alert alert-danger">{{$message}}</div>
   @enderror
   <div class="form-group">
    <label>Bio :</label>
   <textarea name="bio" id="" cols="30"  rows="10" class="form-control" value="{{old('nama')}}" ></textarea>
   </div>
   @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
   @enderror
   <button type="submit" class="btn btn-primary">Submit</button>
 </form>

@endsection