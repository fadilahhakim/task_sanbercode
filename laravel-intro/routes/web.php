<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// router for intro-larvel task
// Route::get('/','HomeController@firstview');
// Route::get('/register','AuthController@signup');
// Route::post('/welcome','AuthController@welcome');

// route for task blade
Route::get('/',function(){
    return view('partials.dasboard');
});
Route::get('/table',function(){
    return view('table');
});
Route::get('/data-table',function(){
    return view('data-table');
});

Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{id}','CastController@show');